﻿// This program reads a code and then
// shows the information about the product you put
using System;

public class Program
{
    public static void Main(string[] args)
    {
        // Variables
        string code;
        string product;
        string origin;
        string ecological;

        // Getting code
        Console.Write("Escriu el codi de producte\n-> ");
        code = Console.ReadLine();

        // Getting product
        product = getProduct(code);
        origin = getOrigin(code);
        ecological = isEcological(code) ? "Sí" : "No";

        Console.WriteLine($"Producte: {product}\nOrigin: {origin}\nEcological: {ecological}");
    }

    // this functions return the product based on the code
    static string getProduct(string c)
    {
        string product = "";

        // Declaring products
        IDictionary<string, string> fruits = new Dictionary<string, string>();
        fruits.Add("00", "citrics");
        fruits.Add("01", "pomes");
        fruits.Add("10", "peres");
        fruits.Add("11", "platans");

        IDictionary<string, string> greens = new Dictionary<string, string>();
        greens.Add("00", "tubercles");
        greens.Add("01", "fulla verda");
        greens.Add("10", "col");
        greens.Add("11", "broquil");

        IDictionary<string, string> meat = new Dictionary<string, string>();
        meat.Add("00", "pollastre");
        meat.Add("01", "vedella");
        meat.Add("10", "xai");
        meat.Add("11", "porc");

        IDictionary<string, string> fish = new Dictionary<string, string>();
        fish.Add("00", "lluç");
        fish.Add("01", "salmó");
        fish.Add("10", "verat");
        fish.Add("11", "sardina");

        /* products["00"] -> fruites{
         *  "00": "Citrics",
         *  "01": "Pomes",
         *  "10": "Peres",
         *  "11": "Platans",
         * } */
        IDictionary<string, IDictionary<string, string>> products = new Dictionary<string, IDictionary<string, string>>();
        products.Add("00", fruits);
        products.Add("01", greens);
        products.Add("10", meat);
        products.Add("11", fish);

        // Obtaining codes
        string productType = c.Substring(0, 2);
        string productCode = c.Substring(2, 2);

        try
        {
            product = products[productType][productCode];
        }
        catch (System.Exception)
        {
            Console.WriteLine("Invalid product code");
            Environment.Exit(1);
        }
        return product;
    }

    // Return the origin based on the code
    static string getOrigin(string c)
    {
        string or = "";
        string originCode = c.Substring(4, 1);

        if (originCode.Equals("0"))
        {
            or = "Europa";
        }
        else if (originCode.Equals("1"))
        {
            or = "Fora d'Europa";
        }
        else
        {
            Console.WriteLine("Invalid origin code");
            Environment.Exit(1);
        }
        return or;
    }

    // Returns a boolean if its ecological or note based on the code
    static bool isEcological(string c)
    {
        string ecoCode = c.Substring(5, 1);

        if (ecoCode.Equals("0"))
        {
            return true;
        }
        else if (ecoCode.Equals("1"))
        {
            return false;
        }
        else
        {
            Console.WriteLine("Invalid ecological code");
            Environment.Exit(1);
        }
        return false;
    }
}
