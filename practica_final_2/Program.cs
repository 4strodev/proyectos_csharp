﻿// Versio .net 6.0.102
public class Program
{
    static void Main(string[] args)
    {
        Dictionary<String, int[]> months = new Dictionary<string, int[]>(){
            {"gener", new int[] {1, 31}},
            {"febrer", new int[] {2, 28}},
            {"març", new int[] {3, 31}},
            {"abril", new int[] {4, 30}},
            {"maig", new int[] {5, 31}},
            {"juny", new int[] {6, 30}},
            {"juliol", new int[] {7, 31}},
            {"agost", new int[] {8, 31}},
            {"septembre", new int[] {9, 30}},
            {"octubre", new int[] {10, 31}},
            {"novembre", new int[] {11, 30}},
            {"desembre", new int[] {12, 31}},
        };

        int currentYear = DateTime.Now.Year;
        int year;
        int day;
        String month;

        while (true)
        {
            try
            {
                // Asking user for a month
                Console.Write("Escriu un mes de l'any: ");
                month = Console.ReadLine()!.Trim().ToLower();
                // Showing month number
                Console.WriteLine("El mes {0} es el numero {1}", month, months[month][0]);
                break;
            }
            catch (Exception e)
            {
                // If month dont exist show error message
                Console.WriteLine("El mes escrit no existeix");
            }
        }

        // Asking for day
        while (true)
        {
            try
            {
                Console.Write("Escriu un dia del mes: ");
                day = int.Parse(Console.ReadLine()!);

                if (day < 1 || day > months[month][1])
                {
                    Console.WriteLine("Dia no valid pel mes introduit");
                    continue;
                }

                break;
            }
            catch (Exception e)
            {
                Console.WriteLine("Has d'introduir un numero");
                continue;
                throw;
            }
        }

        // Asking for year
        while (true)
        {
            try
            {
                Console.Write("Escriu l'any: ");
                year = int.Parse(Console.ReadLine()!);

                if (year <= 1 || year > currentYear)
                {
                    Console.WriteLine("Any fora de rang");
                    continue;
                }

                break;
            }
            catch (Exception e)
            {
                Console.WriteLine("Has d'introduir un numero");
                continue;
                throw;
            }
        }

        // Showing full data
        Console.WriteLine($"La data introduida es {day}/{months[month][0]}/{year}");
    }
}
