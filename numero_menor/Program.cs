﻿using System;

namespace numero_menor
{
    class Program
    {
        static void Main(string[] args)
        {
            // getting numbers from stdin
            Console.Write("Introdueix un numero: ");
            var number1 = Double.Parse(Console.ReadLine());
            Console.Write("Introdueix un altre numero: ");
            var number2 = Double.Parse(Console.ReadLine());

            // checking which is smallest number
            if (number1 == number2)
            {
                Console.WriteLine("Els dos numeros són iguals: ");
            }
            else if (number1 < number2)
            {
                Console.WriteLine($"El nomrbre {number1} es menor que {number2}");
            }
            else
            {
                Console.WriteLine($"El nomrbre {number2} es menor que {number1}");
            }
        }
    }
}
