﻿using System;

//namespace calculadora
//{
    class Program
    {
        static Random random = new Random();
        static void Main(string[] args)
        {
            Console.WriteLine("Aquesta calculadora esta feta per Juan Alejandro Marín Ruiz");

            // Getting number from user
            Console.Write("Escriu un nombre: ");
            var num = Double.Parse(Console.ReadLine());

            // Generating random number and showing to user
            var randNumber = random.Next(1,21);
            Console.WriteLine($"El nombre generat aleatoriament es {randNumber}");

            // Making operations with this numbers
            Console.WriteLine($"{num} + {randNumber} = {num + randNumber}");
            Console.WriteLine($"{num} - {randNumber} = {num - randNumber}");
            Console.WriteLine($"{num} * {randNumber} = {num * randNumber}");
            Console.WriteLine($"{num} / {randNumber} = {num / randNumber}");
        }
    }
//}
