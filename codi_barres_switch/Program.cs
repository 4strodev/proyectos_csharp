﻿// This program reads a code and then
// shows the information about the product you put
using System;

public class Program
{
    public static void Main(string[] args)
    {
        // Variables
        string code;
        string product;
        string origin;
        string ecological;

        // Getting code
        Console.Write("Escriu el codi de producte\n-> ");
        code = Console.ReadLine();

        // Getting product
        product = getProduct(code);
        origin = getOrigin(code);
        ecological = isEcological(code) ? "Sí" : "No";

        Console.WriteLine($"Producte: {product}\nOrigin: {origin}\nEcological: {ecological}");
    }

    // this functions return the product based on the code
    static string getProduct(string c)
    {
        string product = "";
        // Obtaining codes
        string productType = c.Substring(0, 2);
        string productCode = c.Substring(2, 2);

        // Calculating product based on the code
        switch (productType)
        {
            case "00":
                switch (productCode)
                {
                    case "00":
                        product = "citrics";
                        break;
                    case "01":
                        product = "pomes";
                        break;
                    case "10":
                        product = "peres";
                        break;
                    case "11":
                        product = "platans";
                        break;
                    default:
                        Console.WriteLine("Invalid prouct name");
                        Environment.Exit(1);
                        break;
                }
                break;
            case "01":
                switch (productCode)
                {
                    case "00":
                        product = "tubercles";
                        break;
                    case "01":
                        product = "fulla verda";
                        break;
                    case "10":
                        product = "col";
                        break;
                    case "11":
                        product = "broquil";
                        break;
                    default:
                        Console.WriteLine("Invalid prouct name");
                        Environment.Exit(1);
                        break;
                }
                break;
            case "10":
                switch (productCode)
                {
                    case "00":
                        product = "pollastre";
                        break;
                    case "01":
                        product = "vedella";
                        break;
                    case "10":
                        product = "xai";
                        break;
                    case "11":
                        product = "porc";
                        break;
                    default:
                        Console.WriteLine("Invalid prouct name");
                        Environment.Exit(1);
                        break;
                }
                break;
            case "11":
                switch (productCode)
                {
                    case "00":
                        product = "lluç";
                        break;
                    case "01":
                        product = "salmó";
                        break;
                    case "10":
                        product = "verat";
                        break;
                    case "11":
                        product = "sardina";
                        break;
                    default:
                        Console.WriteLine("Invalid prouct name");
                        Environment.Exit(1);
                        break;
                }
                break;
            default:
                Console.WriteLine("Invalid prouct name");
                Environment.Exit(1);
                break;
        }
        return product;
    }

    // Return the origin based on the code
    static string getOrigin(string c)
    {
        string or = "";
        string originCode = c.Substring(4, 1);

        if (originCode.Equals("0"))
        {
            or = "Europa";
        }
        else if (originCode.Equals("1"))
        {
            or = "Fora d'Europa";
        }
        else
        {
            Console.WriteLine("Invalid origin code");
            Environment.Exit(1);
        }

        return or;
    }

    // Returns a boolean if its ecological or note based on the code
    static bool isEcological(string c)
    {
        string ecoCode = c.Substring(5, 1);

        if (ecoCode.Equals("0"))
        {
            return true;
        }
        else if (ecoCode.Equals("1"))
        {
            return false;
        }
        else
        {
            Console.WriteLine("Invalid ecological code");
            Environment.Exit(1);
        }
        return false;
    }
}
