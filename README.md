# **Proyectos C#**

Este repositorio guarda mis ejercicios de la asignatura introducción a la programación.

## **Herramientas recomendadas para trabajar con este repositorio**
- Un editor de codigo recomendable usar [Visual Studio **Code**](https://code.visualstudio.com) con estas extensiones:
    - [C#](https://marketplace.visualstudio.com/items?itemName=ms-dotnettools.csharp)
    - Un tema de agrado

## **Como trabajar con el repositorio**
Si nunca has trabajado con git recomiendo descargar un comprimido con el contenido del repositorio.
![download zip image](/images/download_zip.png)

**Si ya tienes experiencia con git clona el repositorio. Esta es la opcion recomendada**