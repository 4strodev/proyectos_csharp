﻿using System;

namespace factorial
{
    class Program
    {
        static void Main(string[] args)
        {
            // Getting number from user
            Console.Write("Escriu un nombre: ");
            var number = Int32.Parse(Console.ReadLine());
            var factorial = number;

            // Generating factorial
            for (int i = 2; i < number; i++)
            {
                factorial *= i;
            }

            Console.WriteLine($"El factorial de {number} es {factorial}");
        }
    }
}
