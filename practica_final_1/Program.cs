﻿// Versio de .net 6.0.102
public class Program
{
    static void Main(string[] args)
    {
        int upper500 = 0;
        int under500 = 0;
        int equal500 = 0;

        int sum = 0;
        int counter = 0;

        double average;

        // Showing rules
        Console.WriteLine("Has d'introduir nombres positius fins que introdueixis un nombre negatiu. Quan això passi el programa finalitzarà");

        while (true)
        {
            try
            {
                // Asking number to user
                Console.Write("Introdueix un nombre: ");
                int inputNumber = int.Parse(Console.ReadLine()!);

                // if number is negative exit from loop
                if (inputNumber < 0)
                {
                    break;
                }

                // Adding number to total sum
                sum += inputNumber;

                // increasing counter to calculate average
                counter++;

                // Checking range of the number
                if (inputNumber > 500)
                {
                    upper500++;
                }
                else if (inputNumber == 500)
                {
                    equal500++;
                }
                else if (inputNumber < 500)
                {
                    under500++;
                }
                Console.Clear();
            }
            catch (Exception e)
            {
                Console.Clear();
                Console.WriteLine("Has d'introduir un numero");
            }
        }
        // Calculating average
        average = (sum / counter);

        // clearing console
        Console.Clear();

        // Showing data
        Console.WriteLine($"La mitjana es: {average}");
        Console.WriteLine($"Nombres inferiors a 500: {under500}");
        Console.WriteLine($"Nombres superiors a 500: {upper500}");
        Console.WriteLine($"Nombres iguals a 500: {equal500}");
    }
}
