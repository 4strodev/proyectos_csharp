﻿using System;
using System.Collections.Generic;

namespace gelateria_toppings
{
    class Program
    {
        static void Main(string[] args)
        {
            // Establint el sistema de caracters de la consola
            Console.OutputEncoding = System.Text.Encoding.UTF8;

            //Creant mides i preus
            var preusMides = new Dictionary<string, decimal>();
            preusMides.Add("Petit", 1m);
            preusMides.Add("Mitja", 2m);

            //Creant toppings i preus
            var preusToppings = new Dictionary<string, decimal>();
            preusToppings.Add("Oreo", 1m);
            preusToppings.Add("KitKat", 1.50m);
            preusToppings.Add("Brownie", 0.75m);
            preusToppings.Add("Lacasitos", 0.95m);

            // Si un topping ha estat seleccionat es guardarà aquí.
            var toppingsSeleccionats = new Dictionary<string, bool>();
            toppingsSeleccionats.Add("Oreo", false);
            toppingsSeleccionats.Add("KitKat", false);
            toppingsSeleccionats.Add("Brownie", false);
            toppingsSeleccionats.Add("Lacasitos", false);
            
            // Aquestes variables guardaran el import dels gelats
            decimal importTotal = 0;
            decimal importRecipient = 0;
            decimal importToppings = 0;
            decimal descompte = 1;

            // Especificant menus
            string menuMides = $"1. Petit\n2. Mitja";
            string menuToppings = $"1.Oreo\n2.KitKat\n3.Brownie\n4.Lacasitos";

            int recipient;
            // Triant mida gelat
            while (true)
            {
                try
                {
                    Console.WriteLine("Especifiqui una mida pel recipient");
                    Console.WriteLine(menuMides);
                    Console.Write("-> ");
                    recipient = int.Parse(Console.ReadLine());
                    Console.Clear();

                    if (recipient == 1)
                    {
                        importRecipient = preusMides["Petit"];
                    }
                    else if (recipient == 2)
                    {
                        importRecipient = preusMides["Mitja"];
                    }
                    else
                    {
                        throw new RankException();
                    }
                }
                //Si hi ha un error torna a començar el bucle. Si no es així el bucle s'acava
                catch (RankException e)
                {
                    ShowError("Opció no disponible");
                    continue;
                }
                catch
                {
                    // Aquest missatge es mostrara com un "error" per aixo canviem el color a vermell
                    ShowError("Ha d'entrar el numero");
                    continue;
                }
                break;
            }

            string topping;
            // Triant toppings
            bool sortir = false;
            while (!sortir)
            {
                try
                {
                    Console.WriteLine("Especifiqui els toppings que vol. No es pot repetir. Si vol sortir, no escrigui res i premi ENTER.");
                    Console.WriteLine(menuToppings);
                    Console.Write("-> ");

                    topping = Console.ReadLine();
                    Console.Clear();
                    switch (topping)
                    {
                        case "":
                            sortir = true;
                            break;
                        case "1":
                            if (!toppingsSeleccionats["Oreo"])
                            {
                                importToppings += preusToppings["Oreo"];
                                toppingsSeleccionats["Oreo"] = true;
                            }
                            else
                            {
                                ShowError("Topping ja seleccionat");
                            }
                            break;
                        case "2":
                            if (!toppingsSeleccionats["KitKat"])
                            {
                                importToppings += preusToppings["KitKat"];
                                toppingsSeleccionats["KitKat"] = true;

                            }
                            else
                            {
                                ShowError("Topping ja seleccionat");
                            }
                            break;
                        case "3":
                            if (!toppingsSeleccionats["Brownie"])
                            {
                                importToppings += preusToppings["Brownie"];
                                toppingsSeleccionats["Brownie"] = true;
                            }
                            else
                            {
                                ShowError("Topping ja seleccionat");
                            }
                            break;
                        case "4":
                            if (!toppingsSeleccionats["Lacasitos"])
                            {
                                importToppings += preusToppings["Lacasitos"];
                                toppingsSeleccionats["Lacasitos"] = true;
                            }
                            else
                            {
                                ShowError("Topping ja seleccionat");
                            }
                            break;
                        default:
                            throw new RankException();
                    }
                }
                catch (RankException e)
                {
                    ShowError("Opció no disponible");
                    continue;
                }
                catch
                {
                    ShowError("Ha d'entrar el numero");
                    continue;
                }
            }
            // Calculant preu
            importTotal = importRecipient + importToppings;
            // calculant descompte
            if (toppingsSeleccionats["Oreo"] == toppingsSeleccionats["KitKat"] == toppingsSeleccionats["Brownie"] == toppingsSeleccionats["Lacasitos"] && toppingsSeleccionats["Oreo"] == true)
            {
                importTotal = importTotal - descompte;
                Console.WriteLine("S'ha aplicat descompte");
            }

            // Mostrant preu a l'usuari
            Console.WriteLine($"El preu que ha de pagar es {importTotal}.\nImport toppings: {importToppings}.\nImport Gelats: {importRecipient}");
        }

        static void ShowError(string message)
        {
            // Canviant el color perque es mostri com un error
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(message);
            Console.ForegroundColor = ConsoleColor.White;
        }
    }
}
