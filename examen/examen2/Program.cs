﻿// Versió de .NET 6.0
using System;

// Juan Alejandro Marin Ruiz 29/11/21
public class Program
{
    static void Main(string[] args)
    {
        // Variables
        var ranGen = new Random();

        // Generant nombres aleatoris
        var num1 = ranGen.Next(11);
        var num2 = ranGen.Next(11);

        // Mostrant valors
        Console.WriteLine($"El primer valor es: {num1}");
        Console.WriteLine($"El segon valor es: {num2}");
        
        // Mostrant si són iguals o no
        if (num1 == num2)
        {
            Console.WriteLine("Els valors SÓN IGUALS");
        }
        else
        {
            Console.WriteLine("Els valors NO SÓN IGUALS");
        }
    }
}
