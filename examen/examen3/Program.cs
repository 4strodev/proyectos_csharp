﻿// Versió de .NET 6.0
using System;

// Juan Alejandro Marin Ruiz 29/11/21
public class Program
{
    static void Main(string[] args)
    {
        // Variables
        double num1;
        double num2;
        double num3;

        // Demanant numeros per consola
        Console.WriteLine("Introdueix 3 valors numèrics:");
        num1 = double.Parse(Console.ReadLine());
        num2 = double.Parse(Console.ReadLine());
        num3 = double.Parse(Console.ReadLine());

        // Comprovant si han sigut introduits en ordre creixent o no
        if (num1 == num2 && num1 == num3)
        {
            Console.WriteLine("Els valors NO han estat introduits en ordre creixent");
        }
        else if (num1 <= num2 && num2 <= num3)
        {
            Console.WriteLine("Els valors han estat introduits en ordre creixent");
        }
        else
        {
            Console.WriteLine("Els valors NO han estat introduits en ordre creixent");
        }
    }
}
