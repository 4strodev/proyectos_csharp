﻿// Versió de .NET 6.0
using System;

// Juan Alejandro Marin Ruiz 29/11/21
public class Program
{
    static void Main(string[] args)
    {
        // Variables
        string nom;
        string dni;
        int edat;
        int mesNaixament;
        int any = DateTime.Now.Year;
        int anyNaixament;

        // Demanant nom
        Console.Write("Escriu el teu nom: ");
        nom = Console.ReadLine();

        // Demanant dni
        Console.Write("Escriu el teu DNI: ");
        dni = Console.ReadLine();

        // Demanant edat
        Console.Write("Escriu la teva edat: ");
        edat = int.Parse(Console.ReadLine());

        // Demanant mes
        Console.Write("Escriu el teu mes de naixament: ");
        mesNaixament = int.Parse(Console.ReadLine());

        // Calculant any de naixament
        switch (mesNaixament)
        {
            case 12:
                anyNaixament = any - (edat + 1);
                break;
        
            default:
                anyNaixament = any - edat;
                break;
        }

        // Mostrant dades
        Console.WriteLine($"Nom: {nom}\nDNI: {dni}\nEdat: {edat}\nAny de naixament:{anyNaixament}");
    }
}
