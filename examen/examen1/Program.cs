﻿// Versió de .NET 6.0
using System;

// Juan Alejandro Marin Ruiz 29/11/21
class Program
{
    static void Main(string []args)
    {
        // Variables
        string nom;
        string dni;
        string edat;

        // Demanant nom
        Console.Write("Escriu el teu nom: ");
        nom = Console.ReadLine();

        // Demanant dni
        Console.Write("Escriu el teu DNI: ");
        dni = Console.ReadLine();

        // Demanant edat
        Console.Write("Escriu la teva edat: ");
        edat = Console.ReadLine();

        // Mostrant dades
        Console.WriteLine($"Nom: {nom}\nDNI: {dni}\nEdat: {edat}");
    }
}
