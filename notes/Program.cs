﻿using System.Collections;

public class Program
{
    public static void Main(string[] args)
    {
        int nStudentsFailed;
        int nStudentsPassed;

        // Getting number of students
        Console.Write("Digui el numero d'alumnes: ");
        int nAlumnes = Int32.Parse(Console.ReadLine());
        Console.Clear();

        // Creating array to store their notes
        double[] studentsNotes = new double[nAlumnes];

        // Getting students notes
        for (int i = 0; i < studentsNotes.Length; i++)
        {
            Console.Write($"Escrigui la nota del alumne {i + 1}: ");
            studentsNotes[i] = Double.Parse(Console.ReadLine());
            Console.Clear();
        }

        // Getting the average
        double average = getAverage(studentsNotes);

        // Getting max and min
        double min = getMin(studentsNotes);
        double max = getMax(studentsNotes);

        // Getting the number of failed students
        nStudentsFailed = count(studentsNotes, (note) => note < 5);
        nStudentsPassed = count(studentsNotes, (note) => note >= 5);

        // Showing data
        Console.WriteLine($"La mitjana de notes es {average}");

        Console.WriteLine($"La millor nota es: {max}");
        Console.WriteLine($"La pitjor nota es: {min}");

        Console.WriteLine($"El nombre d'estudiants aprobats és {nStudentsPassed}");
        Console.WriteLine($"El nombre d'estudiants suspesos és {nStudentsFailed}");
    }

    static double getAverage(double[] array)
    {
        double total = 0;
        double average;

        // Getting the sum of elements
        foreach (var element in array)
        {
            total += element;
        }

        // Calulating the average
        average = total / array.Length;

        return average;
    }

    // Return the minor element of an array
    static double getMin(double[] array)
    {
        // At the first the min element is the first
        double min = array[0];

        foreach (var element in array)
        {
            // if the current element is minor than the current min change the min
            if (min > element)
            {
                min = element;
            }

        }
        return min;

    }

    // Return the major element of an array
    static double getMax(double[] array)
    {
        // At the first the max element is the first
        double max = array[0];

        foreach (var element in array)
        {
            // if the current element is major than the current max change the max
            if (max < element)
            {
                max = element;
            }

        }

        return max;
    }

    static int count(double[] array, Func<double, bool> rule)
    {
        int i = 0;
        foreach (var item in array)
        {
            if (rule(item))
            {
                i++;
            }
        }

        return i;
    }
}
