﻿// Aquest programa mostra tres opcions

// Per estalviar advertencies d'un possible null value li establim un valor
String option = "";

do
{
    // Mostrem el menu principal
    mostrarMenu();
    option = Console.ReadLine()!;

    // En funcio de l'opcio fem una accio o una altre
    switch (option)
    {
        case "1":
            sum();
            break;
        case "2":
            multTable();
            break;
        case "3":
            numControl();
            break;
        case "4":
            break;
        default:
            System.Console.WriteLine("Opció no disponible");
            break;
    }
}
while (option != "4");

// Mostra el menu principal d'opcions
void mostrarMenu()
{
    var menu = @"Escull una opció:
    1. Suma de numeros positius
    2. Taula de multiplicar
    3. Numero de control
    4. SORTIR";

    Console.WriteLine(menu);
}

// suma numeros fins que s'entri un numero negatiu
void sum()
{
    var sum = 0;
    int input = 0;
    while (input >= 0)
    {
        try
        {
            System.Console.Write("Escriu un numero: ");
            input = int.Parse(Console.ReadLine()!);
            if (input >= 0)
                sum += input;
        }
        catch (Exception e)
        {
            System.Console.WriteLine("Has d'escriure un numero");
        }
    }

    System.Console.WriteLine($"La suma dels numeros entrat es {sum}");
}

// genera una taula de multiplicar
void multTable()
{
    var exit = false;
    while (!exit)
    {
        try
        {
            System.Console.Write("Escriu un numero: ");
            var input = int.Parse(Console.ReadLine()!);
            int i = 0;

            // s'executa do while 
            do
            {
                System.Console.WriteLine($"{input} * {i} = {input * i}");
                i++;
            } while (i <= 10);
            exit = true;
        }
        catch (Exception e)
        {
            System.Console.WriteLine("Has d'escriure un numero");
        }
    }
}

void numControl()
{
    String nums;
    int sum;
    do
    {
        Console.Write("Introdueix un codi: ");
        nums = Console.ReadLine()!;
    } while (nums.Length != 5);

    // Getting control number and sum numbers
    var control_num = int.Parse(nums[nums.Length - 1].ToString());
    var sum_numbers = nums[0..(nums.Length - 1)];

    sum = reduceNumbers(sum_numbers);

    if (sum == control_num) {
        Console.WriteLine("Codi correcte");
    } else {
        Console.WriteLine("Codi incorrecte");
    }
}

int reduceNumbers(string sum_numbers)
{
    int sum;

    do
    {
        sum = 0;

        foreach(var number in sum_numbers){
            sum += int.Parse(number.ToString());
        }

        sum_numbers = sum.ToString();
    }
    while (sum > 9);

    return sum;
}
